module LoadFile
  # A value object representing an entry in a LoadFile
  class LoadFileEntry

    attr_reader :control_number, :volume, :image_path

    def initialize(control_number:, volume:, image_path:)
      @control_number = control_number
      @volume         = volume
      @image_path     = image_path
    end

    # Determine equality of a LoadFileEntry by comparing equality of the control number, volume and
    # image path
    #
    # @param other [LoadFileEntry] a LoadFileEntry to compare equality with the receiver
    # @return [true, false] true if the LoadFileEntries are equal, false otherwise
    def ==(other)
      case other
      when LoadFileEntry
        control_number == other.control_number &&
          volume == other.volume &&
          image_path == other.image_path
      else
        false
      end
    end

    def valid?
      to_validate = %w(path fields control_numbers)

      results = []

      to_validate.each do |type|
        results << send("validate_#{type}")
      end

      results.all?
    end

    private

    def validate_path
      File.exists?(@image_path)
    end

    def validate_fields
      [@control_number, @volume, @image_path].none?{ |x| x.nil? || x.empty? }
    end

    # Assumes control_numbers look like:
    # name-00001
    # where a string is followed by a dash and then 6 digits
    def validate_control_numbers
      number_format = Regexp.new(/^[a-zA-Z]+-\d{6}$/)
      number_format =~ (@control_number)
    end
  end
end
