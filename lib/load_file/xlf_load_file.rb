require_relative 'load_file_entry'
require 'nokogiri'

module LoadFile
  # XlfLoadFile is used to parse XLF format load files
  # XLF format:
  #
  # <loadfile>
  #   <entries>
  #     <entry control-number="test-000001">
  #         <volume>Import Test 01</volume>
  #         <image-path>IMAGES/001/</image-path>
  #         <image-name>test-000001.tif</image-name>
  #      </entry>
  #      <entry control-number="test-000002">
  #         <volume>Import Test 01</volume>
  #         <image-path>IMAGES/001/</image-path>
  #         <image-name>test-000002.tif</image-name>
  #      </entry>
  #      <entry control-number="test-000003">
  #         <volume>Import Test 01</volume>
  #         <image-path>IMAGES/001/</image-path>
  #         <image-name>test-000003.tif</image-name>
  #      </entry>
  #   </entries>
  # </loadfile>
  class XlfLoadFile
    # Initializes an XlfLoadFile instance with an IO object to read from
    #
    # @param io [IO] an IO object to read the loadfile
    #
    # @return [XlfLoadFile] an instance of a XlfLoadFile
    def initialize(io)
      @io = io
    end

    # Implementing the Enumerable interface, calling the block with a LoadFileEntry
    #
    # Reads each XML block from the IO object provided at initialization, creates a new
    # LoadFileEntry from the contents of the line.
    #
    # @return [Enumerator] An Enumerator yielding a LoadFileEntry
    def entries
      load_files = []
      Nokogiri::XML::Reader.from_io(@io).each do |node|
        if node.name == 'entry' && node.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT
          load_files << parse_node(Nokogiri::XML(node.outer_xml).root)
        end
      end

      load_files
    end

    private def parse_node(node)
      control_number = node['control-number']
      children = node.element_children
      volume = children.at('volume').child.to_s
      path = children.at('image-path').child.to_s
      file_name = children.at('image-name').child.to_s
      path = path.chomp('/') # allow for trailing slash
      image_path = "#{path}/#{file_name}"

      LoadFile::LoadFileEntry.new(control_number: control_number,
                                  volume: volume,
                                  image_path: image_path)
    end

    private

    attr_reader :io
  end
end
