require_relative 'load_file_entry'

module LoadFile
  # LfpLoadFile is used to parse LFP format load files
  # LFP format sample:
  #
  # IM,test-000001,S,0,@Import Test 01;IMAGES/001/;test-000001.tif;2,0
  # IM,test-000002,C,0,@Import Test 01;IMAGES/001/;test-000002.tif;2,0
  # IM,test-000003,D,0,@Import Test 01;IMAGES/001/;test-000003.tif;2,0
  class LfpLoadFile
    # Initializes an LfpLoadFile instance with an IO object to read from
    #
    # @param io [IO] an IO object to read the loadfile
    #
    # @return [LfpLoadFile] an instance of a LfpLoadFile
    def initialize(io)
      @io = io
    end

    # Implementing the Enumerable interface, calling the block with a LoadFileEntry
    #
    # Reads each line from the IO object provided at initialization, creates a new
    # LoadFileEntry from the contents of the line.
    #
    # @return [Enumerator] An Enumerator yielding a LoadFileEntry
    def entries
      io.each_line.lazy.map { |line| parse_line(line) }
    end

    private

    def parse_line(line)
      _, control_number, _, _, file_info, _ = line.split(',')
      file_data = parse_file_data(file_info)

      LoadFile::LoadFileEntry.new(control_number: control_number,
                                  volume: file_data[:volume],
                                  image_path: file_data[:path])
    end

    def parse_file_data(file_info)
      parts = file_info.split(';')
      volume = parts.shift.sub('@', '')
      parts.pop # ignore last "column"
      parts[0] = parts[0].chomp('/') # allow for presence of trailing slash in path
      path = parts.join('/')

      {
          volume: volume,
          path: path
      }
    end

    attr_reader :io
  end
end
