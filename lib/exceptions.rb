module LoadFile
  class InvalidFileEntry < StandardError
    def initialize(msg='Invalid File Entry')
      super
    end
  end

  class InvalidLoadFile < StandardError
    def initialize(msg='Invalid Load File')
      super
    end
  end
end