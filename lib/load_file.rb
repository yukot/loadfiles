require_relative 'load_file/opt_load_file'
require_relative 'load_file/lfp_load_file'
require_relative 'load_file/xlf_load_file'
require_relative '../lib/exceptions'


module LoadFile
  # Parse a loadfile in OPT, XLF, or LFP format and return an array of LoadFileEntries
  #
  # @param file_path [String] The load file to use
  # @param type [String] The type of load file
  #
  # @return [Array<LoadFileEntry>]
  # @raise [InvalidFileEntry] entry in load file is invalid
  def self.parse_loadfile(file_path, type)
    entries = []
    File.open(file_path) do |io|
      case type.downcase
        when 'opt'
          loader = OptLoadFile.new(io)
        when 'xlf'
          loader = XlfLoadFile.new(io)
        when 'lfp'
          loader = LfpLoadFile.new(io)
        else
          raise InvalidLoadFile, 'File type must be opt, xlf, or lfp'
      end
      entries = loader.entries
    end

    raise InvalidFileEntry unless entries.all?(&:valid?)

    entries
  end
end