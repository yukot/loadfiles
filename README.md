# LoadFile

## Overview

This assignment is intended to model a typical request to add new features to our document processing pipeline. As part of our team, you will be asked to work independently on such features, select a solution that can be completed in 4-6 hours but not more than 8, and submit that feature for collaborative review by one or more colleagues.

In this assignment, you develop the code required for the solution, and importantly provide a sample pull-request description that addresses all of the considerations in the Deliverables section below. We are looking for colleagues that can not only complete the assignment, but also communicate the pros and cons of their approach clearly and succinctly to thereby contribute to overall team velocity. Place your pull request description in the same directory (i.e. PR.md).

## Context

Logikcull customers can import pre-processed documents into Logikcull by providing an archive in either a ZIP or TAR format. The archive must contain a LoadFile and the actual documents to be imported. The LoadFile contains case-specific information about a document like 'control number' used to uniquely identify documents, a 'volume' to group documents into a collection, and a relative path to the actual document provided in the corresponding archive. When parsing a LoadFile we track 'control number', and 'volume' which are used elsewhere in Logikcull and also the 'image path' which tells us the location of the document in the archive.

## Tasks

1. Logikcull only supports the OPT load file format, already implemented, but many of our customers want to use other common load file formats. We are tasked with modifying our existing code to support additional load file formats while still supporting OPT. Examples of these additional formats can be found below.
2. Logikcull needs to validate the correctness of the load file. At a minimum, Logikcull needs to validate the paths in the load file all have corresponding documents in the archive. To demonstrate that your approach is easily extensible by other developers on the team, implement at least one other validation on the metadata for each loadfile entry such as the presence of all required fields, valid file extensions, or control numbers matching a particular pattern.

## Load File Formats

For the first task we need to add support for the following load file formats. An LFP load file looks like this:

``` csv
IM,test-000001,S,0,@Import Test 01;IMAGES/001/;test-000001.tif;2,0
IM,test-000002,C,0,@Import Test 01;IMAGES/001/;test-000002.tif;2,0
IM,test-000003,D,0,@Import Test 01;IMAGES/001/;test-000003.tif;2,0
```

The first column indicates the type of document being imported. In this case "IM" means the document is an image. For the purposes of this exercise we can ignore this column. The second column is the control number, e.g. "test-000001". The third and fourth column we can also ignore for the purposes of this exercise. The fifth column includes both the volume and the relative path in the archive prefixed with a '@' and separated by a ';'. If we treat the fifth column as its own "semi-colon separated values" the first part is the volume, the second and third make up the relative path of the document in the archive and the fourth is the page count of the document. The page count we can ignore for the purposes of this exercise. Back to the overall LFP file, the sixth column can also be ignored for the purposes of this exercise.

And an XLF load file looks like this:

``` xml
<loadfile>
  <entries>
    <entry control-number="test-000001">
        <volume>Import Test 01</volume>
        <image-path>IMAGES/001/</image-path>
        <image-name>test-000001.tif</image-name>
    </entry>
    <entry control-number="test-000002">
        <volume>Import Test 01</volume>
        <image-path>IMAGES/001/</image-path>
        <image-name>test-000002.tif</image-name>
    </entry>
    <entry control-number="test-000003">
        <volume>Import Test 01</volume>
        <image-path>IMAGES/001/</image-path>
        <image-name>test-000003.tif</image-name>
    </entry>
  </entries>
</loadfile>
```

All load file formats resolve to the following data:

| Control Number | Volume         | Path                       |
| -------------- | -------------- | -------------------------- |
| test-000001    | Import Test 01 | IMAGES/001/test-000001.tif |
| test-000002    | Import Test 01 | IMAGES/001/test-000002.tif |
| test-000003    | Import Test 01 | IMAGES/001/test-000003.tif |

For the purpose of simplicity on the second task we can assume that the contents of the archive have already been extracted to a local filesystem. You don't need to know anything about ZIP or TAR file formats for this exercise.

## Deliverables

This assignment is designed to evaluate how well candiates can work with an existing codebase and how they approach software engineering tasks given a set of constraints. There are no right or wrong answers in this exercise; you are leading this project. We are looking at how candidates think about and implement solutions to problems and are looking for candidates that not only code efficiently but also might engage with our team on the tradeoffs and architectural dilemmas of our craft.

Overall, candidates will be evaluated on the following metrics, **all of which should be addressed by sections in your pull request description**:

* _Completeness_

   Does the candidate complete the tasks to specification? Does the candidate demonstrate their solution meets specification through tests? Take a moment in this section to explain your process, what you did and did not do in order to keep within the time limit, and any other relevant details of how your scoped out the work involved and approached the task.
   
* _Principles_

   Does a candidate apply software engineering best-practices in their implementation and tests? 
   
* _Design Considerations_

   Can a candidate explain why they chose the design submitted and can they explain any alternative solutions considered? If you love functional programming, for example, be sure to explain why you chose an approach different from the the original, what alternatives you considered, and the performance or practical impacts of your choices. 
   
* _Performance_

   Can a candidate explain the performance characteristics of their solution and explain any trade-offs made when implementing their solution?

* _Usability_

   Can a candidate explain any usability issues with their approach for end users of the product who are submitting these archives and responding to any issues with their processing?    
   
* _Maintainability_

   Can a candidate explain any maintainability issues with their approach for the development team?  In a world where two data formats have been requested, how would the team handle requests for additional formats or validation tests?
   
* _Security_

   Can a candidate explain any security concerns with the existing code and their extension of it?  Without adding additional code, what might they recommend adding to this solution to make it potentially safer? 

You should submit your solution as a ZIP or TAR archive or hosted on your favorite SCM provider (e.g. Github, Bitbucket) with all code and documentation that is relevant for us to evaluate your work.

## Build and Test

The LoadFile project requires Ruby 2.4 and bundler. It is up to you to decide how you want you to install Ruby. There is a `.ruby-version` for use with rbenv or rvm. You'll need to install bundler and the gem dependencies before we start.

``` shell
gem install bundler
bundle install --path .vendor --binstubs
```

With Ruby and the gems installed we can run our tests

``` shell
bundle exec rake test
```
