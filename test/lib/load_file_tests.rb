require 'minitest/autorun'
require 'load_file'

class LoadFileTests < Minitest::Test
  def test_load_opt
    opt = <<~EOF
      test-000001,Import Test 01,IMAGES\001\test-000001.tif,Y,,,1
      test-000002,Import Test 01,IMAGES\001\test-000002.tif,Y,,,1
      test-000003,Import Test 01,IMAGES\001\test-000003.tif,Y,,,2
    EOF
    file_path = './loadfile.csv'
    mock_opt = Minitest::Mock.new
    mock_entry1 = Minitest::Mock.new
    mock_entry2 = Minitest::Mock.new
    mock_entry3 = Minitest::Mock.new
    entries = [mock_entry1, mock_entry2, mock_entry3]

    File.stub :open, StringIO.new(opt), StringIO.new(opt) do
      LoadFile::OptLoadFile.stub :new, mock_opt do
        mock_opt.expect :entries, entries
        mock_entry1.expect :valid?, true
        mock_entry2.expect :valid?, true
        mock_entry3.expect :valid? ,true
        LoadFile::parse_loadfile(file_path, 'opt')
        mock_opt.verify
        mock_entry1.verify
        mock_entry2.verify
        mock_entry3.verify
      end
    end
  end

  def test_load_xlf
    xlf = <<~EOF
    <loadfile>
      <entries>
        <entry control-number="test-000001">
            <volume>Import Test 01</volume>
            <image-path>IMAGES/001/</image-path>
            <image-name>test-000001.tif</image-name>
        </entry>
        <entry control-number="test-000002">
            <volume>Import Test 01</volume>
            <image-path>IMAGES/001/</image-path>
            <image-name>test-000002.tif</image-name>
        </entry>
        <entry control-number="test-000003">
            <volume>Import Test 01</volume>
            <image-path>IMAGES/001/</image-path>
            <image-name>test-000003.tif</image-name>
        </entry>
      </entries>
    </loadfile>
    EOF

    file_path = './loadfile.xml'
    mock_xlf = Minitest::Mock.new
    mock_entry1 = Minitest::Mock.new
    mock_entry2 = Minitest::Mock.new
    mock_entry3 = Minitest::Mock.new
    entries = [mock_entry1, mock_entry2, mock_entry3]

    File.stub :open, StringIO.new(xlf), StringIO.new(xlf) do
      LoadFile::XlfLoadFile.stub :new, mock_xlf do
        mock_xlf.expect :entries, entries
        mock_entry1.expect :valid?, true
        mock_entry2.expect :valid?, true
        mock_entry3.expect :valid? ,true
        LoadFile.parse_loadfile(file_path, 'xlf')
        mock_xlf.verify
        mock_entry1.verify
        mock_entry2.verify
        mock_entry3.verify
      end
    end
  end

  def test_load_lfp
    lfp = <<~EOF
    IM,test-000001,S,0,@Import Test 01;IMAGES/001/;test-000001.tif;2,0
    IM,test-000002,C,0,@Import Test 01;IMAGES/001/;test-000002.tif;2,0
    IM,test-000003,D,0,@Import Test 01;IMAGES/001/;test-000003.tif;2,0
    EOF

    file_path = './loadfile.csv'
    mock_lfp = Minitest::Mock.new
    mock_entry1 = Minitest::Mock.new
    mock_entry2 = Minitest::Mock.new
    mock_entry3 = Minitest::Mock.new
    entries = [mock_entry1, mock_entry2, mock_entry3]

    File.stub :open, StringIO.new(lfp), StringIO.new(lfp) do
      LoadFile::LfpLoadFile.stub :new, mock_lfp do
        mock_lfp.expect :entries, entries
        mock_entry1.expect :valid?, true
        mock_entry2.expect :valid?, true
        mock_entry3.expect :valid? ,true
        LoadFile.parse_loadfile(file_path, 'lfp')
        mock_lfp.verify
        mock_entry1.verify
        mock_entry2.verify
        mock_entry3.verify
      end
    end
  end

  def test_invalid_type
    lfp = <<~EOF
    IM,test-000001,S,0,@Import Test 01;IMAGES/001/;test-000001.tif;2,0
    IM,test-000002,C,0,@Import Test 01;IMAGES/001/;test-000002.tif;2,0
    IM,test-000003,D,0,@Import Test 01;IMAGES/001/;test-000003.tif;2,0
    EOF

    file_path = './loadfile.csv'
    File.stub :open, StringIO.new(lfp), StringIO.new(lfp) do
      assert_raises(LoadFile::InvalidLoadFile) { LoadFile.parse_loadfile(file_path, 'foo') }
    end
  end
end